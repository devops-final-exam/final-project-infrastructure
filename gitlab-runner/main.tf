resource "aws_key_pair" "keypair" {
  key_name   = var.username
  public_key = file(var.ssh_key_path)
}

resource "aws_instance" "finalServer" {
  ami           = var.my_ami
  instance_type = var.instance_type

  key_name      = aws_key_pair.keypair.key_name

  user_data = <<-EOF
    #!/bin/bash
    sudo adduser ansible
    sudo addgroup ansible
    sudo usermod -aG sudo ansible
  EOF

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file(var.ssh_secret_key_path)
    }

    inline = [
        "echo Hello World! > hello.txt"
    ]
  }
}
