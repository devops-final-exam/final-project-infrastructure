variable "location" {
  type        = string
  default     = "us-east-1"
  description = "This is location our server"
}

variable "username" {
  type        = string
  default     = "dima@dimas-laptop"
  description = "This is username my pc"
}

variable "ssh_key_path" {
  type        = string
  default     = "~/.ssh/id_rsa.pub"
  description = "This is my path ssh key public" 
}

variable "ssh_secret_key_path" {
  type        = string
  default     = "~/.ssh/id_rsa"
  description = "This is my path ssh key sercet" 
}

variable "my_ami" {
  type        = string
  default     = "ami-0fc5d935ebf8bc3bc"
  description = "This is my server ami" 
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "This is type our server" 
}